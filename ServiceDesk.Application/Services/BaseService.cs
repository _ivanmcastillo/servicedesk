﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ServiceDesk.Application.Interfaces;
using ServiceDesk.Data.ServiceDesk.Domain.Models;
using ServiceDesk.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ServiceDesk.Application.Services
{
    public class BaseService<T, Dto> : IBaseService<T, Dto>
        where T : class, IBaseEntity
        where Dto : class, IBaseEntity

    {
        protected readonly IMapper _mapper;
        protected readonly ServiceDeskContext _context;
        public BaseService(IMapper mapper, ServiceDeskContext context)
        {
            _mapper = mapper;
            _context = context;
        }
        public async Task<Dto> AddAsync(Dto item)
        {
            item.FechaModificacion = DateTime.Now;
            item.FechaRegistro = DateTime.Now;
            T entity = _context.Set<T>().Add(_mapper.Map<T>(item)).Entity;
            return await _context.SaveChangesAsync() > 0 ? _mapper.Map<Dto>(entity) : null;
        }

        public async Task<bool> Delete(int id)
        {
            T entity = await _context.Set<T>().FindAsync(id);
            entity.Borrado = true;
            entity.FechaModificacion = DateTime.Now;
            _context.Set<T>().Update(entity);
            return await _context.SaveChangesAsync() > 0;
        }

        public async Task<Dto> Edit(Dto item)
        {
            item.FechaModificacion = DateTime.Now;
            T entity = _context.Set<T>().Update(_mapper.Map<T>(item)).Entity;
            return await _context.SaveChangesAsync() > 0 ? _mapper.Map<Dto>(entity) : null;
        }

        public async Task<ICollection<Dto>> Get(params string[]? includes)
        {
            IQueryable<T> query = _context.Set<T>().Where(b => b.Borrado == false).AsNoTracking().AsQueryable();
            foreach (string item in includes)
            {
                query.Include(item);
            }

            return _mapper.Map<List<Dto>>(await query.ToListAsync());
        }

        public async Task<Dto> GetById(int id)
        {
            T entity = await _context.Set<T>().FindAsync(id);
            return _mapper.Map<Dto>(entity);
        }

        public async Task<ICollection<Dto>> GetItems(Expression<Func<T, bool>> predicate, params string[]? includes)
        {
            IQueryable<T> query = _context.Set<T>().Where(predicate).AsNoTracking().AsQueryable();
            foreach (string item in includes)
            {
                query.Include(item);
            }

            return _mapper.Map<List<Dto>>(await query.ToListAsync());
        }
    }
}
