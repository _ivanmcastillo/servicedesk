﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ServiceDesk.Application.Interfaces
{
    public interface IBaseService<T, Dto>
        where T : class
        where Dto : class
    {
        Task<Dto> AddAsync(Dto item);
        Task<Dto> GetById(int id);
        Task<ICollection<Dto>> GetItems(Expression<Func<T, bool>> predicate, params string[]? includes);
        Task<ICollection<Dto>> Get(params string[]? includes);
        Task<bool> Delete(int id);
        Task<Dto> Edit(Dto item);
    }
}
