﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceDesk.Domain.Models
{
    public interface IBaseEntity
    {
        public bool Borrado { get; set; }
        public string ModificadoPor { get; set; }
        public string CreadoPor { get; set; }
        public DateTimeOffset FechaRegistro { get; set; }
        public DateTimeOffset FechaModificacion { get; set; }
    }
}
