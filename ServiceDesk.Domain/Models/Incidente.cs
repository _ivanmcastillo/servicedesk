﻿using ServiceDesk.Domain.Models;
using System;
using System.Collections.Generic;

#nullable disable

namespace ServiceDesk.Data.ServiceDesk.Domain.Models
{
    public partial class Incidente : IBaseEntity
    {
        public Incidente()
        {
            HistorialIncidentes = new HashSet<HistorialIncidente>();
        }

        public int Id { get; set; }
        public string UsuarioReportaId { get; set; }
        public string UsuarioAsignadoId { get; set; }
        public int PrioridadId { get; set; }
        public int DepartamentoId { get; set; }
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public DateTimeOffset FechaCierre { get; set; }
        public string ComentarioCierre { get; set; }
        public bool Estatus { get; set; }
        public bool Borrado { get; set; }
        public DateTimeOffset FechaRegistro { get; set; }
        public DateTimeOffset FechaModificacion { get; set; }
        public string CreadoPor { get; set; }
        public string ModificadoPor { get; set; }

        public virtual Usuario CreadoPorNavigation { get; set; }
        public virtual Departamento Departamento { get; set; }
        public virtual Usuario ModificadoPorNavigation { get; set; }
        public virtual Prioridad Prioridad { get; set; }
        public virtual Usuario UsuarioAsignado { get; set; }
        public virtual Usuario UsuarioReporta { get; set; }
        public virtual ICollection<HistorialIncidente> HistorialIncidentes { get; set; }
    }
}
