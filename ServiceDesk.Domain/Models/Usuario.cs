﻿using Microsoft.AspNetCore.Identity;
using ServiceDesk.Domain.Models;
using System;
using System.Collections.Generic;

#nullable disable

namespace ServiceDesk.Data.ServiceDesk.Domain.Models
{
    public partial class Usuario : IdentityUser, IBaseEntity
    {
        public Usuario()
        {
            DepartamentoCreadoPorNavigations = new HashSet<Departamento>();
            DepartamentoModificadoPorNavigations = new HashSet<Departamento>();
            HistorialIncidenteCreadoPorNavigations = new HashSet<HistorialIncidente>();
            HistorialIncidenteModificadoPorNavigations = new HashSet<HistorialIncidente>();
            IncidenteCreadoPorNavigations = new HashSet<Incidente>();
            IncidenteModificadoPorNavigations = new HashSet<Incidente>();
            IncidenteUsuarioAsignados = new HashSet<Incidente>();
            IncidenteUsuarioReporta = new HashSet<Incidente>();
            InverseCreadoPorNavigation = new HashSet<Usuario>();
            InverseModificadoPorNavigation = new HashSet<Usuario>();
            PrioridadCreadoPorNavigations = new HashSet<Prioridad>();
            PrioridadModificadoPorNavigations = new HashSet<Prioridad>();
            PuestoCreadoPorNavigations = new HashSet<Puesto>();
            PuestoModificadoPorNavigations = new HashSet<Puesto>();
            SlaCreadoPorNavigations = new HashSet<Sla>();
            SlaModificadoPorNavigations = new HashSet<Sla>();
        }
        public int PuestoId { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Cedula { get; set; }
        public string Correo { get; set; }
        public string Telefono { get; set; }
        public DateTime FechaNacimiento { get; set; } = DateTime.Now;
        public bool Estatus { get; set; }
        public bool Borrado { get; set; }
        public DateTimeOffset FechaRegistro { get; set; } = DateTime.Now;
        public DateTimeOffset FechaModificacion { get; set; } = DateTime.Now;
        public string CreadoPor { get; set; }
        public string ModificadoPor { get; set; }

        public virtual Usuario CreadoPorNavigation { get; set; }
        public virtual Usuario ModificadoPorNavigation { get; set; }
        public virtual Puesto Puesto { get; set; }
        public virtual ICollection<Departamento> DepartamentoCreadoPorNavigations { get; set; }
        public virtual ICollection<Departamento> DepartamentoModificadoPorNavigations { get; set; }
        public virtual ICollection<HistorialIncidente> HistorialIncidenteCreadoPorNavigations { get; set; }
        public virtual ICollection<HistorialIncidente> HistorialIncidenteModificadoPorNavigations { get; set; }
        public virtual ICollection<Incidente> IncidenteCreadoPorNavigations { get; set; }
        public virtual ICollection<Incidente> IncidenteModificadoPorNavigations { get; set; }
        public virtual ICollection<Incidente> IncidenteUsuarioAsignados { get; set; }
        public virtual ICollection<Incidente> IncidenteUsuarioReporta { get; set; }
        public virtual ICollection<Usuario> InverseCreadoPorNavigation { get; set; }
        public virtual ICollection<Usuario> InverseModificadoPorNavigation { get; set; }
        public virtual ICollection<Prioridad> PrioridadCreadoPorNavigations { get; set; }
        public virtual ICollection<Prioridad> PrioridadModificadoPorNavigations { get; set; }
        public virtual ICollection<Puesto> PuestoCreadoPorNavigations { get; set; }
        public virtual ICollection<Puesto> PuestoModificadoPorNavigations { get; set; }
        public virtual ICollection<Sla> SlaCreadoPorNavigations { get; set; }
        public virtual ICollection<Sla> SlaModificadoPorNavigations { get; set; }
    }
}
