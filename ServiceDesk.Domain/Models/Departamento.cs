﻿using ServiceDesk.Domain.Models;
using System;
using System.Collections.Generic;

#nullable disable

namespace ServiceDesk.Data.ServiceDesk.Domain.Models
{
    public partial class Departamento : IBaseEntity
    {
        public Departamento()
        {
            Incidentes = new HashSet<Incidente>();
            Puestos = new HashSet<Puesto>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public bool Estatus { get; set; }
        public bool Borrado { get; set; }
        public DateTimeOffset FechaRegistro { get; set; }
        public DateTimeOffset FechaModificacion { get; set; }
        public string CreadoPor { get; set; }
        public string ModificadoPor { get; set; }
        public virtual Usuario CreadoPorNavigation { get; set; }
        public virtual Usuario ModificadoPorNavigation { get; set; }
        public virtual ICollection<Incidente> Incidentes { get; set; }
        public virtual ICollection<Puesto> Puestos { get; set; }
    }
}
