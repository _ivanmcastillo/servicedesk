﻿using ServiceDesk.Domain.Models;
using System;
using System.Collections.Generic;

#nullable disable

namespace ServiceDesk.Data.ServiceDesk.Domain.Models
{
    public partial class Sla : IBaseEntity
    {
        public Sla()
        {
            Prioridads = new HashSet<Prioridad>();
        }

        public int Id { get; set; }
        public string Descripcion { get; set; }
        public int CantidadHoras { get; set; }
        public bool Estatus { get; set; }
        public bool Borrado { get; set; }
        public DateTimeOffset FechaRegistro { get; set; }
        public DateTimeOffset FechaModificacion { get; set; }
        public string CreadoPor { get; set; }
        public string ModificadoPor { get; set; }

        public virtual Usuario CreadoPorNavigation { get; set; }
        public virtual Usuario ModificadoPorNavigation { get; set; }
        public virtual ICollection<Prioridad> Prioridads { get; set; }
    }
}
