﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ServiceDesk.Application.Interfaces;
using ServiceDesk.Data.ServiceDesk.Domain.Models;
using ServiceDesk.Shared.ViewModels;

namespace ServiceDesk.Server.Controllers
{
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    [ApiController]
    public class DepartamentoController : InitialController<Departamento, DepartamentoDto>
    {
        public DepartamentoController(IBaseService<Departamento, DepartamentoDto> _service)
            : base(_service)
        {
        }
    }
}
