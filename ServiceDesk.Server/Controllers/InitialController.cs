﻿using ServiceDesk.Application.Interfaces;
using ServiceDesk.Server.Controllers.Interfaces;
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ServiceDesk.Domain.Models;

namespace ServiceDesk.Server.Controllers
{
    [System.Web.Http.Route("api/[controller]")]
    [ApiController]
    public class InitialController<T, Dto> : ControllerBase, IInitialController<T, Dto>
        where T : class, IBaseEntity
        where Dto : class
    {
        protected IBaseService<T, Dto> _service;
        public InitialController(IBaseService<T, Dto> service)
        {
            _service = service;
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            if (id > 0)
            {
                try
                {
                    return Ok(await _service.Delete(id));
                }
                catch (Exception ex)
                {
                    return StatusCode(500, ex.Message);
                }
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                return Ok(await _service.Get());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            if (id > 0)
            {
                try
                {
                    return Ok(await _service.GetById(id));
                }
                catch (Exception ex)
                {
                    return StatusCode(500, ex.Message);
                }
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post(Dto item)
        {
            if (item != null)
            {
                Dto itemToAdd = await _service.AddAsync(item);
                if (itemToAdd != null)
                {
                    return Ok(item);
                }
                else
                {
                    return BadRequest();
                }
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpPut]
        public async Task<IActionResult> Put(Dto item)
        {
            if (item != null)
            {
                Dto itemToUpdate = await _service.Edit(item);
                if (itemToUpdate != null)
                {
                    return Ok(item);
                }
                else
                {
                    return BadRequest();
                }
            }
            else
            {
                return BadRequest();
            }
        }


    }
}
