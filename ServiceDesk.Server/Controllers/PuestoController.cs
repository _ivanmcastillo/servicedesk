﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServiceDesk.Application.Interfaces;
using ServiceDesk.Data.ServiceDesk.Domain.Models;
using ServiceDesk.Shared.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceDesk.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PuestoController : InitialController<Puesto, PuestoDto>
    {
        public PuestoController(IBaseService<Puesto, PuestoDto> service)
            : base(service)
        {

        }
    }
}
