﻿using Microsoft.AspNetCore.Mvc;
using ServiceDesk.Application.Interfaces;
using System.Threading.Tasks;

namespace ServiceDesk.Server.Controllers.Interfaces
{
    public interface IInitialController<T, Dto>
        where T : class
        where Dto : class
    {
        Task<IActionResult> Post(Dto item);
        Task<IActionResult> Put(Dto item);
        Task<IActionResult> Delete(int id);
        Task<IActionResult> GetAll();
        Task<IActionResult> GetById(int id);
    }
}
