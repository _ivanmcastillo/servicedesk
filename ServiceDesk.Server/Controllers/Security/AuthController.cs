﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using ServiceDesk.Data.ServiceDesk.Domain.Models;
using ServiceDesk.Shared.Security;
using ServiceDesk.Shared.ViewModels;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace ServiceDesk.Server.Controllers.Security
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly UserManager<Usuario> _userManager;
        private readonly SignInManager<Usuario> _signInManager;
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;
        private readonly ServiceDeskContext _context;

        public AuthController(
            UserManager<Usuario> userManager,
            SignInManager<Usuario> signInManager,
            IConfiguration configuration,
            IMapper mapper,
            ServiceDeskContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = configuration;
            _mapper = mapper;
            _context = context;
        }

        [HttpPost("Crear")]
        public async Task<ActionResult<UserToken>> CreateUser([FromBody] UserInfo model)
        {
            var user = _mapper.Map<Usuario>(model.Usuario);
            user.FechaModificacion = DateTime.Now;
            user.FechaRegistro = DateTime.Now;
            user.UserName = model.Username;
            var result = await _userManager.CreateAsync(user, model.Password);
            if (result.Succeeded)
            {
                return BuildToken(model);
            }
            else
            {
                return BadRequest("Username or password invalid");
            }

        }

        [HttpPut("modificar")]
        public async Task<ActionResult<bool>> ModifyUser([FromBody] UserInfo model)
        {
            Usuario entity = (await _userManager.Users.Where(b => b.Id.ToLower() == model.Usuario.Id.ToLower()).AsNoTracking().FirstOrDefaultAsync());
            entity.UserName = model.Username;
            entity.FechaModificacion = DateTime.Now;
            entity.Nombre = model.Usuario.Nombre;
            entity.Apellido = model.Usuario.Apellido;
            entity.Correo = model.Usuario.Correo;
            entity.Cedula = model.Usuario.Cedula;
            entity.Telefono = model.Usuario.Telefono;

            _context.Attach(entity);

            var result = await _userManager.UpdateAsync(entity);
            if (result.Succeeded)
            {
                return Ok(true);
            }
            else
            {
                Console.WriteLine();
                return BadRequest("Error al actualizar usuario");
            }

        }

        [HttpGet]
        public async Task<List<UsuarioDto>> Get() 
        {
            return _mapper.Map<List<UsuarioDto>>(await _userManager.Users.Include("Puesto").AsNoTracking().ToListAsync()); 
        }

        [HttpGet("[action]/{UserId}")]
        public async Task<UserInfo> GetById([FromRoute]string UserId)
        {
            Usuario entity = (await _userManager.Users.Where(b => b.Id.ToLower() == UserId.ToLower()).Include("Puesto").AsNoTracking().FirstOrDefaultAsync());
            UserInfo userInfo = new UserInfo();
            UsuarioDto user = _mapper.Map<UsuarioDto>(entity);
            userInfo.Username = entity.UserName;
            userInfo.Usuario = user;
            return userInfo;
        }

        [HttpPost("Login")]
        public async Task<ActionResult<UserToken>> Login([FromBody] UserInfo userInfo)
        {
            try
            {
                var result = await _signInManager.PasswordSignInAsync(userInfo.Username, userInfo.Password, isPersistent: false, lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    return BuildToken(userInfo);
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                    return BadRequest(ModelState);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
                throw;
            }

        }

        private UserToken BuildToken(UserInfo userInfo)
        {
            var user = _userManager.Users.Where(b => b.UserName == userInfo.Username).FirstOrDefault();
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.UniqueName, userInfo.Username),
                new Claim(ClaimTypes.Name, userInfo.Username),
                new Claim("Id", user.Id),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var expiration = DateTime.UtcNow.AddYears(1);

            JwtSecurityToken token = new JwtSecurityToken(
               issuer: null,
               audience: null,
               claims: claims,
               expires: expiration,
               signingCredentials: creds);

            return new UserToken()
            {
                Token = new JwtSecurityTokenHandler().WriteToken(token),
                Expiration = expiration
            };
        }
    }
}
