﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ServiceDesk.Application.Interfaces;
using ServiceDesk.Data.ServiceDesk.Domain.Models;
using ServiceDesk.Shared.ViewModels;

namespace ServiceDesk.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IncidenteController : InitialController<Incidente, IncidenteDto>
    {
        public IncidenteController(IBaseService<Incidente, IncidenteDto> service) 
            :base(service)
        {

        }
    }
}
