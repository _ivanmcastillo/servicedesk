﻿namespace ServiceDesk.Presentation.Models
{
    public class DrawerItem
    {
        public string Text { get; set; }

        public string Icon { get; set; }

    }
}
