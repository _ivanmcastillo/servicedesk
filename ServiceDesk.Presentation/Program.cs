using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using ServiceDesk.Presentation.Agents;
using ServiceDesk.Presentation.ClientServices;
using ServiceDesk.Presentation.Helpers;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ServiceDesk.Presentation
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<App>("#app");

            string baseAddress = builder.Configuration.GetValue<string>("BaseUrl");
            builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(baseAddress) });

            builder.Services.AddTelerikBlazor();

            builder.Services.AddAuthorizationCore();
            builder.Services.AddScoped<AuthService>();
            builder.Services.AddScoped<AuthenticationStateProvider, AuthService>(provider => provider.GetRequiredService<AuthService>());
            builder.Services.AddScoped<ILoginService, AuthService>(provider => provider.GetRequiredService<AuthService>());

            builder.Services.AddScoped<LoginAgent>();
            builder.Services.AddScoped<IUriHelper, UriHelper>();

            builder.Services.AddScoped<DepartamentoAgent>();
            builder.Services.AddScoped<PuestosAgent>();
            builder.Services.AddScoped<PrioridadAgent>();
            builder.Services.AddScoped<SlaAgent>();
            builder.Services.AddScoped<UsuarioAgent>();
            builder.Services.AddScoped<IncidenteAgent>();


            await builder.Build().RunAsync();
        }
    }
}
