﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceDesk.Presentation.ClientServices
{
    public interface ILoginService
    {

        Task Login(string token);
        Task Logout();

    }
}
