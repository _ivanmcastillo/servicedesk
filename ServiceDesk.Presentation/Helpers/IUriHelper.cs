﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceDesk.Presentation.Helpers
{
    public interface IUriHelper
    {
        public void NavigateTo(string uri, bool forceLoad = false);
    }
}
