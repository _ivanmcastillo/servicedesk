﻿using ServiceDesk.Shared.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ServiceDesk.Presentation.Agents
{
    public class SlaAgent : BaseAgent<SlaDto>
    {
        public SlaAgent(HttpClient httpClient)
            : base(httpClient, "api/sla/")
        {

        }
    }
}
