﻿using ServiceDesk.Shared.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ServiceDesk.Presentation.Agents
{
    public class IncidenteAgent : BaseAgent<IncidenteDto>
    {
        public IncidenteAgent(HttpClient httpClient)
            : base(httpClient, "/api/Incidente")
        {

        }


    }
}
