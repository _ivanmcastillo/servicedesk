﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceDesk.Presentation.Agents.Interfaces
{
    public interface IBaseAgent<Dto> where Dto : class
    {
        public Task<Dto> Add(Dto item);
        public Task<Dto> Edit(Dto item);
        public Task<bool> Remove(int id);
        public Task<List<Dto>> Get();
        public Task<List<Dto>> GetById(int id);

    }
}
