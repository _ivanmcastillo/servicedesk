﻿using ServiceDesk.Shared.ViewModels;
using System.Net.Http;

namespace ServiceDesk.Presentation.Agents
{
    public class DepartamentoAgent : BaseAgent<DepartamentoDto>
    {
        public DepartamentoAgent(HttpClient httpClient)
            :base(httpClient, "api/departamento/")
        {

        }
    }
}
