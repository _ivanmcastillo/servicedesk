﻿using ServiceDesk.Presentation.Agents.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace ServiceDesk.Presentation.Agents
{
    public class BaseAgent<Dto> : IBaseAgent<Dto> where Dto : class
    {
        protected readonly HttpClient _httpCient;
        protected string BaseUrl;
        public BaseAgent(HttpClient httpCient, string baseUrl)
        {
            _httpCient = httpCient;
            BaseUrl = baseUrl;
        }
        public virtual async Task<Dto> Add(Dto item)
        {
            var result = await _httpCient.PostAsJsonAsync<Dto>(BaseUrl, item);

            return await result.Content.ReadFromJsonAsync<Dto>();
        }

        public virtual async Task<Dto> Edit(Dto item)
        {
            var result = await _httpCient.PutAsJsonAsync<Dto>(BaseUrl, item);

            return await result.Content.ReadFromJsonAsync<Dto>();
        }

        public virtual async Task<List<Dto>> Get()
        {
            var result = await _httpCient.GetFromJsonAsync<List<Dto>>(BaseUrl);

            return result;
        }

        public virtual async Task<List<Dto>> GetById(int id)
        {
            var result = await _httpCient.GetFromJsonAsync<List<Dto>>($"{BaseUrl}{id}");

            return result;
        }

        public virtual async Task<bool> Remove(int id)
        {
            var result = await _httpCient.DeleteAsync($"{BaseUrl}{id}");

            return result.IsSuccessStatusCode;
        }
    }
}
