﻿using ServiceDesk.Shared.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ServiceDesk.Presentation.Agents
{
    public class PuestosAgent : BaseAgent<PuestoDto>
    {
        public PuestosAgent(HttpClient httpClient)
            : base(httpClient, "api/puesto/")
        {

        }
    }
}
