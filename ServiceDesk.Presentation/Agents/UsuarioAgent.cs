﻿using ServiceDesk.Shared.Security;
using ServiceDesk.Shared.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace ServiceDesk.Presentation.Agents
{
    public class UsuarioAgent : BaseAgent<UsuarioDto>
    {
        public UsuarioAgent(HttpClient httpClient)
            :base(httpClient, "api/auth/")
        {

        }

        public virtual async Task<bool> Modificar(UserInfo item)
        {
            var result = await _httpCient.PutAsJsonAsync($"{BaseUrl}modificar", item);

            return await result.Content.ReadFromJsonAsync<bool>();
        }

        public virtual async Task<UserToken> CreateUser(UserInfo item)
        {
            var result = await _httpCient.PostAsJsonAsync($"{BaseUrl}crear", item);

            return await result.Content.ReadFromJsonAsync<UserToken>();
        }

        public virtual async Task<UserInfo> GetById(string UserId)
        {
            var result = await _httpCient.GetFromJsonAsync<UserInfo>($"{BaseUrl}getbyid/{UserId}");

            return result;
        }
    }
}
