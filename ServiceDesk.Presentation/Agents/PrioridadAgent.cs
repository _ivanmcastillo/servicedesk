﻿using ServiceDesk.Shared.ViewModels;
using System.Net.Http;

namespace ServiceDesk.Presentation.Agents
{
    public class PrioridadAgent : BaseAgent<PrioridadDto>
    {
        public PrioridadAgent(HttpClient httpClient)
            : base(httpClient, "api/prioridad/")
        {

        }
    }
}
