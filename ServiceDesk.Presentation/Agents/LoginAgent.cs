﻿using ServiceDesk.Presentation.ClientServices;
using ServiceDesk.Shared.Security;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using System;

namespace ServiceDesk.Presentation.Agents
{
    public class LoginAgent
    {
        private readonly HttpClient _httpClient;
        private readonly ILoginService _loginService;
        public LoginAgent(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<string> Login(UserInfo userInfo)
        {
            var result = await _httpClient.PostAsJsonAsync<UserInfo>("api/auth/Login", userInfo);
            UserToken userToken = await result.Content.ReadFromJsonAsync<UserToken>();

            return userToken.Token;
        }
    }
}
