﻿using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace ServiceDesk.Data.ServiceDesk.Domain.Models
{
    public partial class ServiceDeskContext : IdentityDbContext<Usuario>
    {
        public ServiceDeskContext(DbContextOptions<ServiceDeskContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Departamento> Departamentos { get; set; }
        public virtual DbSet<HistorialIncidente> HistorialIncidentes { get; set; }
        public virtual DbSet<Incidente> Incidentes { get; set; }
        public virtual DbSet<Prioridad> Prioridads { get; set; }
        public virtual DbSet<Puesto> Puestos { get; set; }
        public virtual DbSet<Sla> Slas { get; set; }
        public virtual DbSet<Usuario> Usuarios { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer();
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);

            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Departamento>(entity =>
            {
                entity.ToTable("Departamento");

                entity.HasIndex(e => e.CreadoPor, "IX_Departamento_CreadoPor");

                entity.HasIndex(e => e.ModificadoPor, "IX_Departamento_ModificadoPor");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.CreadoPorNavigation)
                    .WithMany(p => p.DepartamentoCreadoPorNavigations)
                    .HasForeignKey(d => d.CreadoPor)
                    .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(d => d.ModificadoPorNavigation)
                    .WithMany(p => p.DepartamentoModificadoPorNavigations)
                    .HasForeignKey(d => d.ModificadoPor)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<HistorialIncidente>(entity =>
            {
                entity.ToTable("HistorialIncidente");

                entity.HasIndex(e => e.CreadoPor, "IX_HistorialIncidente_CreadoPor");

                entity.HasIndex(e => e.IncidenteId, "IX_HistorialIncidente_IncidenteId");

                entity.HasIndex(e => e.ModificadoPor, "IX_HistorialIncidente_ModificadoPor");

                entity.Property(e => e.Comentario)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.HasOne(d => d.CreadoPorNavigation)
                    .WithMany(p => p.HistorialIncidenteCreadoPorNavigations)
                    .HasForeignKey(d => d.CreadoPor)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Incidente)
                    .WithMany(p => p.HistorialIncidentes)
                    .HasForeignKey(d => d.IncidenteId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.ModificadoPorNavigation)
                    .WithMany(p => p.HistorialIncidenteModificadoPorNavigations)
                    .HasForeignKey(d => d.ModificadoPor)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Incidente>(entity =>
            {
                entity.ToTable("Incidente");

                entity.HasIndex(e => e.CreadoPor, "IX_Incidente_CreadoPor");

                entity.HasIndex(e => e.DepartamentoId, "IX_Incidente_DepartamentoId");

                entity.HasIndex(e => e.ModificadoPor, "IX_Incidente_ModificadoPor");

                entity.HasIndex(e => e.PrioridadId, "IX_Incidente_PrioridadId");

                entity.HasIndex(e => e.UsuarioAsignadoId, "IX_Incidente_UsuarioAsignadoId");

                entity.HasIndex(e => e.UsuarioReportaId, "IX_Incidente_UsuarioReportaId");

                entity.Property(e => e.ComentarioCierre)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.Descripcion).IsRequired();

                entity.Property(e => e.Titulo)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.HasOne(d => d.CreadoPorNavigation)
                    .WithMany(p => p.IncidenteCreadoPorNavigations)
                    .HasForeignKey(d => d.CreadoPor)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Departamento)
                    .WithMany(p => p.Incidentes)
                    .HasForeignKey(d => d.DepartamentoId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.ModificadoPorNavigation)
                    .WithMany(p => p.IncidenteModificadoPorNavigations)
                    .HasForeignKey(d => d.ModificadoPor)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Prioridad)
                    .WithMany(p => p.Incidentes)
                    .HasForeignKey(d => d.PrioridadId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.UsuarioAsignado)
                    .WithMany(p => p.IncidenteUsuarioAsignados)
                    .HasForeignKey(d => d.UsuarioAsignadoId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.UsuarioReporta)
                    .WithMany(p => p.IncidenteUsuarioReporta)
                    .HasForeignKey(d => d.UsuarioReportaId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Prioridad>(entity =>
            {
                entity.ToTable("Prioridad");

                entity.HasIndex(e => e.CreadoPor, "IX_Prioridad_CreadoPor");

                entity.HasIndex(e => e.ModificadoPor, "IX_Prioridad_ModificadoPor");

                entity.HasIndex(e => e.SlaId, "IX_Prioridad_SlaId");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.CreadoPorNavigation)
                    .WithMany(p => p.PrioridadCreadoPorNavigations)
                    .HasForeignKey(d => d.CreadoPor)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.ModificadoPorNavigation)
                    .WithMany(p => p.PrioridadModificadoPorNavigations)
                    .HasForeignKey(d => d.ModificadoPor)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Sla)
                    .WithMany(p => p.Prioridads)
                    .HasForeignKey(d => d.SlaId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Puesto>(entity =>
            {
                entity.ToTable("Puesto");

                entity.HasIndex(e => e.CreadoPor, "IX_Puesto_CreadoPor");

                entity.HasIndex(e => e.DepartamentoId, "IX_Puesto_DepartamentoId");

                entity.HasIndex(e => e.ModificadoPor, "IX_Puesto_ModificadoPor");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.CreadoPorNavigation)
                    .WithMany(p => p.PuestoCreadoPorNavigations)
                    .HasForeignKey(d => d.CreadoPor)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Departamento)
                    .WithMany(p => p.Puestos)
                    .HasForeignKey(d => d.DepartamentoId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.ModificadoPorNavigation)
                    .WithMany(p => p.PuestoModificadoPorNavigations)
                    .HasForeignKey(d => d.ModificadoPor)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Sla>(entity =>
            {
                entity.ToTable("Sla");

                entity.HasIndex(e => e.CreadoPor, "IX_Sla_CreadoPor");

                entity.HasIndex(e => e.ModificadoPor, "IX_Sla_ModificadoPor");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.HasOne(d => d.CreadoPorNavigation)
                    .WithMany(p => p.SlaCreadoPorNavigations)
                    .HasForeignKey(d => d.CreadoPor)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.ModificadoPorNavigation)
                    .WithMany(p => p.SlaModificadoPorNavigations)
                    .HasForeignKey(d => d.ModificadoPor)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Usuario>(entity =>
            {
                entity.ToTable("Usuario");

                entity.HasIndex(e => e.CreadoPor, "IX_Usuario_CreadoPor");

                entity.HasIndex(e => e.ModificadoPor, "IX_Usuario_ModificadoPor");

                entity.HasIndex(e => e.PuestoId, "IX_Usuario_PuestoId");

                entity.Property(e => e.Apellido)
                    .HasMaxLength(100);

                entity.Property(e => e.Cedula)
                    .HasMaxLength(11);


                entity.Property(e => e.Correo)
                    .HasMaxLength(50);

                entity.Property(e => e.FechaNacimiento).HasColumnType("datetime");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(100);


                entity.Property(e => e.Telefono)
                    .HasMaxLength(15);

                entity.HasOne(d => d.CreadoPorNavigation)
                    .WithMany(p => p.InverseCreadoPorNavigation)
                    .HasForeignKey(d => d.CreadoPor)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.ModificadoPorNavigation)
                    .WithMany(p => p.InverseModificadoPorNavigation)
                    .HasForeignKey(d => d.ModificadoPor)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Puesto)
                    .WithMany(p => p.Usuarios)
                    .HasForeignKey(d => d.PuestoId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
