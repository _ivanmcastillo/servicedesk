﻿using ServiceDesk.Domain.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceDesk.Shared.ViewModels
{
    public class UsuarioDto : IBaseEntity
    {
        [Required(ErrorMessage = "Este campo es requerido")]
        public string Id { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Escoja un puesto válido")]
        public int PuestoId { get; set; }
        [Required(ErrorMessage = "Este campo es requerido")]
        public string Nombre { get; set; }
        [Required(ErrorMessage = "Este campo es requerido")]
        public string Apellido { get; set; }
        [Required(ErrorMessage = "Este campo es requerido")]
        public string Cedula { get; set; }
        [Required(ErrorMessage = "Este campo es requerido")]
        public string Correo { get; set; }

        [Required(ErrorMessage = "Este campo es requerido")]
        public string Telefono { get; set; }
        [Required]
        [Range(typeof(DateTime), "01/01/1900", "01/01/2020", ErrorMessage ="Inserte una fecha válida")]
        public DateTime FechaNacimiento { get; set; }
        public bool Estatus { get; set; }
        public bool Borrado { get; set; }
        public DateTimeOffset FechaRegistro { get; set; }
        public DateTimeOffset FechaModificacion { get; set; }
        public string CreadoPor { get; set; }
        public string ModificadoPor { get; set; }
        public string UserName { get; set; }

        public virtual PuestoDto Puesto { get; set; }
    }
}
