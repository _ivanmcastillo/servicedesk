﻿using ServiceDesk.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceDesk.Shared.ViewModels
{
    public class PuestoDto : IBaseEntity
    {
        public int Id { get; set; }
        public int DepartamentoId { get; set; }
        public string Nombre { get; set; }
        public bool Estatus { get; set; }
        public bool Borrado { get; set; }
        public DateTimeOffset FechaRegistro { get; set; }
        public DateTimeOffset FechaModificacion { get; set; }
        public string CreadoPor { get; set; }
        public string ModificadoPor { get; set; }
    }
}
