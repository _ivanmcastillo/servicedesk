﻿using AutoMapper;
using ServiceDesk.Data.ServiceDesk.Domain.Models;
using ServiceDesk.Shared.ViewModels;

namespace ServiceDesk.Shared.MappingProfile
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Departamento, DepartamentoDto>();
            CreateMap<DepartamentoDto, Departamento>();

            CreateMap<Puesto, PuestoDto>();
            CreateMap<PuestoDto, Puesto>();

            CreateMap<Prioridad, PrioridadDto>();
            CreateMap<PrioridadDto, Prioridad>();

            CreateMap<Sla, SlaDto>();
            CreateMap<SlaDto, Sla>();

            CreateMap<Usuario, UsuarioDto>();
            CreateMap<UsuarioDto, Usuario>();

            CreateMap<Incidente, IncidenteDto>();
            CreateMap<IncidenteDto, Incidente>();

        }
    }
}
