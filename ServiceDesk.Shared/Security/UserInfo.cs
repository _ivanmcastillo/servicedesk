﻿using ServiceDesk.Shared.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceDesk.Shared.Security
{
    public class UserInfo
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }

        public UsuarioDto Usuario { get; set; } = new UsuarioDto();

    }
}
